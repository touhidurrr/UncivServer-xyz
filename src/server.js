require('dotenv').config();
const express = require('express');
const { rmSync, writeFileSync } = require('fs');

// error logger
const errorLogger = e => console.error(e.stack);

// express
var server = express();
server.disable('x-powered-by');

server.get('/isalive', async (req, res) => {
  res.set('Content-Type', 'text/plain');
  res.end('true');
});

server.use(function (req, res, next) {
  if (
    !req.path.startsWith('/assets') &&
    (req.path.startsWith('/src') ||
      req.path.startsWith('/node_modules') ||
      req.path.endsWith('.js') ||
      req.path.endsWith('.json'))
  ) {
    res.sendStatus(403);
    return;
  }
  if (req.path.startsWith('/files')) {
    res.set('Content-Type', 'text/plain');
  }
  next();
});

server.use(express.static('.', { limit: '5mb', lastModified: false }));

// Limit, 3 MegaBytes
// 1 << 10 = 1024 << 10 = 1024 * 1024
const limit = 3 << 20;

// Text Body Parser
server.use(function (req, res, next) {
  let ln = req.get('content-length');
  if (ln && ln > limit) {
    res.sendStatus(413);
    return;
  }
  req.body = '';
  let overLimit = false;
  req.on('data', chunk => {
    if (overLimit) return;
    req.body += chunk;
    if (req.body.length > limit) {
      overLimit = true;
      res.sendStatus(413);
      return;
    }
  });
  req.on('end', () => {
    if (!overLimit) next();
  });
});

server.put('/files/:fileName', async (req, res) => {
  if (!req.body) {
    console.dir(req);
    res.sendStatus(400);
    return;
  }

  writeFileSync(req.path.slice(1), req.body);
});

server.delete('/files/:fileName', async (req, res) => {
  rmSync(req.path.slice(1), { force: true });
  res.sendStatus(200);
});

// Start Server
(async () => {
  // start server
  server.listen(process.env.PORT || 8080, async () => {
    console.dir(`Listening on ${process.env.PORT || 8080} ...`);
  });
})();

// error handler
process.on('error', errorLogger);
